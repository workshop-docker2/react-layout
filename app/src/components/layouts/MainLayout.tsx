import Navbar from '../shared/Navbar'
import Footer from '../shared/Footer'

const MainLayout = ({ children }: any) => {
  return (
    <>

    <Navbar />

    { children }

    <Footer />

    </>
  )
}

export default MainLayout